<?php get_header(); ?>

 
<div class="bottom-front-page">
  <div class="about-us">
    <!-- Query and show a page based on the page ID -->
    <?php $aboutUs = new WP_Query('page_id=10'); ?>
    <?php while($aboutUs->have_posts()): $aboutUs->the_post(); ?>
      <h2><?php the_title(); ?></h2>
      <?php the_content(); ?>
    <?php endwhile; wp_reset_postdata(); ?>
  </div>

  <div class="blog-tips">
    <h2>Tips to travel to Toronto</h2>
    <!-- Custom query to show the latest 2 blog posts -->
    <?php $args = array(
      'cat' => 4,  // The blog post category id
      'posts_per_page' => 2,
      'order' => 'DESC', // Show the latest blog post
      'orderby' => 'date'
    ); ?>
    <?php $travelTips = new WP_Query($args); ?>
    <ul>
      <?php while($travelTips->have_posts()): $travelTips->the_post(); ?>
        <li>
        <a href="<?php the_permalink(); ?>">
          <?php the_post_thumbnail('medium-blog'); ?>
        </a>
        <h3><?php the_title(); ?></h3>
        <?php the_excerpt(); ?>
        <a class="read-more" href="<?php the_permalink(); ?>">
          Continue reading
        </a>
        </li>
      <?php endwhile; wp_reset_postdata(); ?>
    </ul>
  </div>
</div>


<?php get_footer(); ?>

Hello from front-page.php