<?php
function torontoOnline_scripts() {

  // Add style.css
	wp_enqueue_style('style', get_stylesheet_uri());

	// Add jQuery
	wp_enqueue_script('jquery');
	
	// Add JS files
	wp_enqueue_script('scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array('jquery'), '1.0', true);
}

add_action('wp_enqueue_scripts', 'torontoOnline_scripts');

// Remove Admin bar on the top
add_filter('show_admin_bar', '__return_false');


// Add menu
register_nav_menus(array(
	'main_menu' => __('Main Menu', 'torontoOnline')
) );


// Add widget zone
function theme_widgets() {
	register_sidebar(array(
		'name' 		  => __('Sidebar Testimonials'),
		'id'		  => 'testimonials',
		'description' => 'Testimonials Widgets',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title'  => '</h3>',
	) );
}
add_action('widgets_init', 'theme_widgets');


// Add featured image
add_theme_support('post-thumbnails');

// Set image size
add_image_size('featured', 1100, 418, true); // true= crop the image
add_image_size('medium-blog', 358, 208, true); 
 