      </div>
      <!-- End container -->
    </div>
    <!-- End #page -->


    <footer class="site-footer" role="contentinfo">
      <div class="container">
        <nav id="footer-navigation" class="footer-navigation" role="navigation">
          <?php wp_nav_menu(array('theme_location' => 'main_menu'));  // main_menu = nama menu di functions.php ?>
        </nav>
      </div>
			<hr>
			<div class="copyright">
				<p>torontoOnline <?php echo date('Y'); ?></p>
			</div>
		</footer>
    <?php wp_footer(); ?>
  </body>
</html>